import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("src/main/resources/quiz.txt");
        List<SingleQuizQuestion> allQuestions = readAllQuizeQuestionsFromFile(file);
        playQuizGame(allQuestions);
    }

    private static void playQuizGame(List<SingleQuizQuestion> allQuestions) {
        System.out.println("Welcome to Quiz Game!");
        Collections.shuffle(allQuestions);
        int points = 0;

        for (int j = 0; j < 10; j++) {
            SingleQuizQuestion quizQuestion = allQuestions.get(j);
            boolean hasUserAnsweredCorrectly = askSingleQuizQuestion(quizQuestion);
            if (hasUserAnsweredCorrectly) {
                points++;
            }
        }
        System.out.println("Congrats! Your score is " + points + "/10");
    }

    private static boolean askSingleQuizQuestion(SingleQuizQuestion quizQuestion) {
        System.out.println("Question: " + quizQuestion.getQuestion());
        List<String> possibleAnswers = quizQuestion.getAllAnswersShuffled();
        for (int i = 0; i < possibleAnswers.size(); i++) {
            String ans = possibleAnswers.get(i);
            System.out.println("Answer " + (i + 1) + ") " + ans);
        }
        int userAnswerIndex = readNumberFromUser(1, 4) - 1;

        String userAnswerText = possibleAnswers.get(userAnswerIndex);
        System.out.println("Your choice: " + userAnswerText);
        String goodAnswer = quizQuestion.getCorrectAnswer();
        if (userAnswerText.equals(goodAnswer)) {
            System.out.println("Correct!");
            return true;
        } else {
            System.out.println("Bad one, correct is: " + goodAnswer);
            return false;
        }
    }


    private static int readNumberFromUser(int minimalAllowedNumber, int maxAllowedNumber) {
        Scanner scannerFromKeyboard = new Scanner(System.in);
        int userAnswerIndex;
        do {
            System.out.print("Select answer: ");
            try {
                userAnswerIndex = Integer.parseInt(scannerFromKeyboard.nextLine());
            } catch (NumberFormatException e) {
                userAnswerIndex = -1;
            }
            if (userAnswerIndex < minimalAllowedNumber || userAnswerIndex > maxAllowedNumber) {
                System.out.println("This is not a valid answer! Pick number from " + minimalAllowedNumber + " to " + maxAllowedNumber + "!");
            }
        } while (userAnswerIndex < minimalAllowedNumber || userAnswerIndex > maxAllowedNumber);
        return userAnswerIndex;
    }


    private static List<SingleQuizQuestion> readAllQuizeQuestionsFromFile(File file) throws FileNotFoundException {
        Scanner scaner = new Scanner(file);
        List<SingleQuizQuestion> allQuizQuestions = new ArrayList<SingleQuizQuestion>();

        //why hasNext???
        while (scaner.hasNext()) {
            String question = scaner.nextLine();
            String correctAnswer = scaner.nextLine();
            List<String> badAnswers = new ArrayList<String>(3000);
            for (int i = 0; i < 3; i++) {
                String badAnswer = scaner.nextLine();
                badAnswers.add(badAnswer);
            }
             SingleQuizQuestion qq = new SingleQuizQuestion();
            qq.setQuestion(question);
            qq.setCorrectAnswer(correctAnswer);
            qq.setBadAnswers(badAnswers);
            allQuizQuestions.add(qq);
        }

        return allQuizQuestions;
    }
}
