import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SingleQuizQuestion {

    private String question;
    private String correctAnswer;
    private List<String> badAnswers;

    @Override
    public String toString() {
        return "SingleQuizQuestion{" +
                "question='" + question + '\'' +
                ", correctAnswer='" + correctAnswer + '\'' +
                ", badAnswers=" + badAnswers +
                '}';
    }



    public SingleQuizQuestion(String question, String correctAnswer, List<String> badAnswers) {
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.badAnswers = badAnswers;
    }

    public SingleQuizQuestion() {

    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public void setBadAnswers(List<String> badAnswers) {
        this.badAnswers = badAnswers;
    }

    public String getQuestion() {
        return question;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public List<String> getBadAnswers() {
        return badAnswers;
    }

    public List<String> getAllAnswersShuffled() {
        List<String> possibleAnswers = new ArrayList<String>();
        possibleAnswers.add(correctAnswer);
        possibleAnswers.addAll(badAnswers);
        Collections.shuffle(possibleAnswers);
        return possibleAnswers;

    }
}
